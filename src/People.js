import React, { Component } from 'react';

export default class People extends Component {
  constructor(props) {
    super(props);
    this.state = {
      people: []
    }
  }

  componentDidMount() {
    fetch('http://swapi.co/api/people/?format=json')
      .then(response => response.json())
      .then(data => this.setState({
          people: data.results
        })
      )
      .catch(error => console.error(error));
  }

  render() {
    return (
      <div>
        <h1>People</h1>
        {this.state.people
          .map(person =>
            <p key={person.name}>{person.name}</p>
          )
        }
      </div>
    )
  }
}

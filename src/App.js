import React, { Component } from 'react';
import People from './People';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Star War</h2>
        </div>
        <People />
      </div>
    );
  }
}

export default App;
